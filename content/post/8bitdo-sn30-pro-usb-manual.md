---
title: "8BitDo SN30 Pro USB Manual"
date: 2019-08-15T13:19:58+02:00
draft: False
---

The manual that 8bitdo offers is not complete so I decided to write down what I found out about the controller. For example, the manual does not tell you that you can indeed turn the vibration feature on or off. I thought that a firmware update might fix it but it actually didn't.

<br>

## Key Combinations (while plugged in)

**L + R + Select**: Toggles vibration

**Star + Any key**: Toggles turbo feature for that button.

<br>

## Key Combinations (while plugging in)

**L + R**: Firmware update mode

This exposes a USB drive that a firmware file can be copied on. You can get them from [https://support.8bitdo.com](https://support.8bitdo.com). It did not work on Linux in my case and the controller wouldn't start up after copying the file on. It did work when I copied the .dat file with Windows XP, though.