---
title: "Firefox Kinetic Scrolling"
date: 2020-04-07T12:54:44+02:00
---

Depending on your laptop's touchpad and your driver you can scroll smoothly (pixel-wise, not row-wise) through applications. Most GTK applications support it but also Telegram Desktop and others. This is pretty close to the scroll experience you get on macOS.

Firefox can also be set to use that feature which makes it much easier to scroll to the top or bottom of a page by a flick.

To do that you need to add the following line to your `/etc/environment` file.

```
MOZ_USE_XINPUT2=1
```

Just paste it as a new line and you're good to go. Now you only need to **disable** smooth scrolling in Firefox (<kbd><kbd>Preferences</kbd> → <kbd>Use smooth scrolling</kbd></kbd>). Then either reboot and enjoy the feature permanently enabled or open a terminal, enter `export MOZ_USE_XINPUT2=1` and then `firefox` to test it immediately.

---

I tried this on Ubuntu Studio 19.10 with XFCE on X. This should work by default if you run Firefox under Wayland.
