---
title: "Clockwork Carnage Devlog #2: Splitscreen and Controllers"
date: 2019-07-22T16:33:12+02:00
draft: false
---

During the past few days I've been working on 2-player **splitscreen** and **controller support** for the game. Configurating controllers can be the most confusing thing. Nowadays many controllers just copy the layout of XBox controllers and even the 8bitdo controller I just got maps to that exact layout. That simplifies the configuration but there's still one problem: It's hard to remember which controller is bound to which player.

![Split screen, two cars](/cwc/2019-07-28_02.png)

So right now the game recognizes any game controller and treats it with the default XBox layout. To enter the game you just have to press any button to activate the controller and you're in. Since the keyboard also controls the main menu you have to press Enter to play using the keyboard.  
That means that there's no setting for the number of players, it's determined dynamically by how many buttons have been pressed. There's going to be a settings menu to configure the controller schemes per-controller. The cool thing about this is that controllers aren't bound to players which makes it a bit easier, players can just take any controller and play with it.  
The last thing I need to figure out for this is how to leave the game again. At the moment I'm thinking about a button that has to be clicked with the controller that the player doesn't want to play with.

![Main menu prototype](/cwc/2019-07-28_01.png)

Other than that I implemented a pretty awesome **rumble** feature that reacts to how much you drift. Re-Volt only has limited rumble support, vibration kicks in at 100% whenever you're going over a bumpy ground which is sad because it's such a cool feature. I'm also going to add rumble for collisions and some feedback for UI. This way you can exactly feel when your vehicle starts drifting or when your powerup is ready, for example.

Another thing I've been working on is **game modes**. Right now there's only one static game mode that gets loaded in as a node. It runs a script that searches for Area objects (those define out-of-bounds areas) and triggers a function that takes note of cars that fell off the platform.  
Later when custom levels are supported, creators just have to place a simple box below the level. If the car falls into this box, the game takes note of it on the results list. The last car to remain on the platform wins. This is going to be a lot more exciting when there are weapons that you can use to blow other up but for now it technically works:

![Splitscreen, one half showing a car falling, the other still on the platform. Below is console output text showing that player 1 wins.](/cwc/2019-07-28_03.png)

---

[<- Devlog #1](/post/clockwork-carnage-devlog-001/)