---
title: "Customizing Firefox for a Simpler Browsing Experience"
date: 2020-10-02T19:03:01+02:00
draft: false
---

## Introduction

The other day we set up an iPad for my partner's grandma. It's been years since I've been through the setup process. I used to have a second generation iPod touch which I used a ton and even jailbroke it. It was the first proper mobile operating system that let you install software, browse the internet freely and stay in touch with others.
I remember the user experience to be very simple. Almost every app followed a similar scheme and you quickly understood how everything works intuitively.
Even though this is not the main topic of this post, I feel like I still need to mention some points since they are relevant to my main point.

There is a lot of nonsense that you can't really opt out of. We searched for some games (Solitaire, for example) and installed only the ones that did not offer in-app purchases and came without ads.
Upon opening them we found out that they in fact don't come with apps or in-app purchases. However, every time you start up the app you're greeted with the iOS game center prompt asking you to log in. There's no way to disable that. So instead I just enabled the game center functionality, setting up a user ID for someone who does not even care about playing competitively against others.
That's when I was shocked to find out that the app did actually have advertisements. You can disable them with a one-time purchase. This is only the case when you're signed in with a game center ID. Sooo... Either have a prompt come up every time you start the game (something that might confuse grandma) or have ads displayed (something I did not want since it also causes confusion). I just uninstalled the app.

Today my partner got a call from her grandma. She wanted to look up how to store beets. We went through the procedure of googling with her which made all the hurdles apparent.
Every website informs you about cookies with screen-blocking pop-ups. To someone who does not use the internet that much this is just distracting and confusing. The concept of cookies does not even if you only want to research how certain vegetables are best stored. Of course, on top of that the internet is also littered with ads, something that I don't experience at all since I use an ad blocker.

Having problems navigating a user interface has nothing to do with the age of the user. Users aren't too old to use something. It's rather that user interfaces are too inaccessible to people who depend on their intuition. Today's UIs assume that you know your way around which is an accessibility problem, just like websites assume that you can navigate and read websites with your eyes.
All this really made me want to do something. I can't do much about the bad UX that many current operating systems come with. However, I can at least customize a web browser in a way so that websites become less distracting and more accessible.

## List of Firefox Add-Ons

This is an expanding list of Firefox add-ons that simplify the browsing experience:


### uBlock Origin

This add-on is a lightweight ad blocker that works nicely out of the box. You can enable additional lists that cater certain languages and types of advertisements.

- [Link to uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)


### I don't care about cookies

This add-on aims to remove cookie notifications on many websites.

- [Link to I don't care about cookies](https://addons.mozilla.org/en-US/firefox/addon/i-dont-care-about-cookies/)


### Startpage

Startpage is a search engine that gets its results from Google. However, their website is simpler and they don't collect personalized data. This extension sets up Startpage as the default search engine and shows it in the new tab page (which also removes visual clutter).

- [Link to Startpage add-on](https://addons.mozilla.org/en-US/firefox/addon/startpage-private-search-tab/)
