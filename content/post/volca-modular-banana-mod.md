---
title: "Volca Modular Banana Mod"
date: 2021-06-09
draft: false
---

> To be expanded in the future

![](/img/volca/01 photo_2020-08-18_00-20-19.jpg)

I started finding a layout that I like using Blender.

![](/img/volca/02 photo_2020-08-18_07-49-09.jpg)

I then ended up with something that's pretty close to the final layout.

![](/img/volca/03 photo_2020-08-03_00-15-19.jpg)

Before starting to build, I needed to make sure that everything I had planned actually works in practice. The keyboard uses capacitive sensing and I wanted to use thumbscrews. I tried booting the volca with a screwdriver on one pad. Touching the screwdriver now correctly triggered the key (even though you could hover a bit over it since some of the accuracy was lost).

![](/img/volca/04 photo_2020-08-03_00-18-29.jpg)

My plan was to strip all jacks and potentiometers from the PCB. The potentiometers' solder points are quite big and easy to find.

![](/img/volca/05 photo_2020-08-03_00-19-07.jpg)

The jacks' solder points are a bit smaller.

![](/img/volca/06 photo_2020-08-22_18-14-34.jpg)

I started prototyping with a thin MDF board and some DIY cables.

![](/img/volca/07 photo_2020-08-22_18-14-50.jpg)

I got the jacks second hand.

![](/img/volca/08photo_2020-08-22_18-54-10.jpg)

I used a compass to draw the jack and knob locations, pretty much eyeballing it just to see if the layout makes sense.

![](/img/volca/09 photo_2020-08-22_18-58-26.jpg)

Some random knobs for visualization.

![](/img/volca/10 photo_2020-08-23_18-57-35.jpg)

Testing if cables are in the way (a bit but the knobs can still be reached well enough).

![](/img/volca/11 photo_2020-08-23_18-57-42.jpg)

Bottom with most jacks installed.

![](/img/volca/12 photo_2020-08-23_18-57-53.jpg)

Side view

![](/img/volca/Screenshot_20201227-104152.jpg)

![](/img/volca/Screenshot_20201227-104156.jpg)

![](/img/volca/Screenshot_20201227-104200.jpg)

![](/img/volca/13 photo_2020-10-10_14-58-26.jpg)

It was probably both the worst and best decision to go with olive wood. It smells and looks nice but it's really dense and hard to work with. I also don't have the right tools to work with harder woods (but that didn't stop me).

![](/img/volca/14 photo_2020-11-16_21-01-12.jpg)

One of the thumbscrews I'm going to use.

![](/img/volca/15 photo_2020-11-16_20-59-52.jpg)

Checking were to put the PCBs.

![](/img/volca/16 Screenshot_20201227-104146.jpg)

The wood was really rough so sanding took a long time. In the end it still had some lines but I thought it was good enough.



![](/img/volca/Screenshot_20201227-104208.jpg)


I drew the exact layout on paper.![](/img/volca/Screenshot_20201227-104512.jpg)


![](/img/volca/Screenshot_20201227-104518.jpg)


When I was sure about the layout, I started drilling holes for the jacks.

![](/img/volca/Screenshot_20201227-104525.jpg)


![](/img/volca/Screenshot_20201227-104529.jpg)
![](/img/volca/Screenshot_20201227-104534.jpg)
![](/img/volca/Screenshot_20201227-104538.jpg)
![](/img/volca/Screenshot_20201227-104542.jpg)
![](/img/volca/Screenshot_20201227-104546.jpg)



![](/img/volca/photo_2020-12-05_09-26-50.jpg)![](/img/volca/Screenshot_20201227-104559.jpg)


![](/img/volca/Screenshot_20201227-104603.jpg)
I took apart a LAN cable and used it to connect all points on the PCB to the potentiometers and jacks.

![](/img/volca/Screenshot_20201227-104607.jpg)


![](/img/volca/photo_2020-12-18_18-20-16.jpg)![](/img/volca/Screenshot_20201227-104611.jpg)
![](/img/volca/Screenshot_20201227-104616.jpg)

It kept getting harder and harder as the board was getting more and more attached to the case by the wires.



![](/img/volca/IMG_20210319_1115548.jpg)

The final wiring. You can see the MIDI board in the top right.



![](/img/volca/vmb.jpg)

I closed off the bottom with an oiled piece of plywood



![](/img/volca/129773194_491408888495462_4479571820299670220_n.jpg)

From left to right:

- audio out
- sync out
- sync in
- CV in
- CV in (banana)
- CV in (banana)
- MIDI in
- MIDI out
- DC





![](/img/volca/file:///home/marv/VMB Build Log/unsorted/Screenshot_20201227-104551.jpg)

![](/img/volca/Screenshot_20201227-104555.jpg)
