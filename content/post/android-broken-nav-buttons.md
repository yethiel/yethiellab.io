---
title: "What to do when your hardware navigation buttons on Android don't work"
date: 2021-08-24T15:54:46+02:00
draft: false
---

Ever since my first smartphone in 2010 (an HTC Wildfire), I always retired phones because their memory was starting to get really slow (Nexus S) or because their battery was giving up (even bloating the device in one case, Samsung Note 2). Another phone didn't always receive calls (Blackberry Priv) which made me miss a date and some calls of my then new boss. Knowing that the Blackberry KeyOne was produced by TCL and not Blackberry (and probably didn't suffer from ignored calls), I went for that one. I had it for 3 years: Perfect camera, perfect keyboard, *amazing* battery life (lasting me up to four days). The other day it spectacularly jumped out of my backpack and, for the first time, I picked up a phone with a completely shattered screen.
The screen works and mostly reacts to touches, except for the navigation buttons. This means I'm stuck in one app at a time (until I connect to it via [scrcpy](https://github.com/Genymobile/scrcpy))


An impulse buy later I'm now waiting for a Fairphone. After purchasing I realized that shipping might take up to a month so I wondered if there is any way to make the software navigation buttons show up on an unrooted Android phone (since you cannot root the KeyOne). There seems to be no way to do that, however, I found this wonderful piece of software: [VirtualSoftKeys](https://f-droid.org/en/packages/tw.com.daxia.virtualsoftkeys/).

VirtualSoftKeys might not enable the actual navigation bar on Android but it does a pretty good job replacing it on a broken phone. Thanks a ton for the author of this app.

	
--- 

Out of all phones I mentioned, the Wildfire still functions best.