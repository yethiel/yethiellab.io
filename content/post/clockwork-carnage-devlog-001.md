---
title: "Clockwork Carnage Devlog #1"
date: 2019-07-10T18:46:01+02:00
---

Now that my first game [BriZide](https://yethiel.itch.io/brizide) is all done and published I want to dive into a new engine. Blender Game Engine was nice but now since it's been deprecated I wanted to finish BriZide as quickly as possible. It didn't become what I initially planned but, then again, I didn't even have a game design document either.

The thing that's most important to me is that my games are as inclusive and as customizable as possible.  
**Inclusive** means that it's free, open and playable by as many people as possible. It should also run on as many computer as possible. That doesn't mean that I'm going to do huge sacrifices in terms of graphical quality. I think configurability (offering different display options, compatibility with multiple renderers) is a good way to make a game playable on older machines as well.  
**Moddability** is the other point that's important to me. Many of today's games don't allow for easy modification. Either they're protected by anti-piracy measures or their assets are in proprietary and undocumented formats. [Re-Volt](https://re-volt.io) taught me how amazing modding communities can be and how talents can be born if a game is easy to mod and offers many points of entry for different skill levels.

I experimented a lot with BriZide and most of its development time went into making up a structure that makes the game modular and customizable. Ships consist of Blender models and a simple text file, place them in a folder and it's in the game. Levels can be created in Blender as well and are saved in text files. You could just take out the *ship* component and replace it with a first person shooter player.  
It's close to what I want to achieve but Blender's game engine was limited: It couldn't generate meshes on the fly and once stuff got complex issues popped up. I ended up using hot glue all over the place which made the game less stable.

[Godot](https://godotengine.org) reminds me of BGE a lot so it was easy for me to apply my skills there. It also offers many features that I missed in BGE. For example, my current prototype is able to import PRM (Re-Volt) meshes and the default car physics are also stable enough for my new game.

Here's a screenshot of what I have so far:

![Clockwork Carnage prototype screenshot](/cwc/Screenshot from 2019-07-10 19-03-59.png)

The car information is loaded from a file called `parameters.json` which is similar to Re-Volt's parameters.txt. Here's an excerpt:

```json
{
    "name": "Clockwork Bus",
    "author": "URV",
    "description": "The bus is nice.",
    "date": "2019-06-28",
    "revision": 2,
    "type": "vehicle/car",

    "engine_force": 20,
    "steer_angle": 0.22,
    "steer_speed": 1.0,

    "body": {
        "mesh": "/cars/winbus/body.prm",
        "texture": "/cars/winbus/car.png",
        "mass": 8.62,
        "gravity_scale": 1.0,
        "offset": [0.0, 0.0, 0.0],
        "rotation": [0.0, 0.0, 0.0],
        "scale": [1.0, 1.0, 1.0]
    },

    "wheels": [
        {
            "name": "wheelfl",
            "mesh": "/cars/winbus/wheelfl.prm",
            "texture": "/cars/winbus/car.png",
            "position": [-0.16, 0.0, 0.286],
            "offset": [0.0, 0.0, 0.0],
            "scale": [1.0, 1.0, 1.0],
            "steer": true,
            "powered": true,
            "radius": 0.12,
[...]
```
The file includes wheel position and all handling information required by Godot's VehicleBody node. There's going to be some extra stuff like car horns, engine sounds, etc.

The game's content directory (where all user content goes, too) currently looks like this:

![Screenshot of prototype file structure](/cwc/Screenshot from 2019-07-10 19-06-38.png)

I'm not exactly sure where this project is going. My current idea is to create a relatively simple RV spinoff that focuses on game modes that aren't really implemented in RVGL, for example Last Man Standing (platform battles), soccer and more. Single player modes are not going to be a priority but are definitely possible in the future. Players are also able to create their own game modes, after all.

There are some major points that need to be addressed, though. Since it's going to be a multiplayer-focused game, I need to make sure that cheating is kept at a minimum. That means the server has to whitelist allowed cars and compare them with a checksum. For now I'll focus on creating local multiplayer, though. I ordered a controller to be able to test it better, can't wait for it to arrive!

If you're curios (heck, if you want to contribute), the game is open source and on my [GitLab profile](https://gitlab.com/yethiel).

That's all I have so far. There are tons of ideas I'll tell you in the upcoming devlogs!

---

[Devlog #2 ->](/post/clockwork-carnage-devlog-002/)