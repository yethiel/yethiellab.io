---
title: "004.05"
date: 2019-05-29T10:34:47+02:00
film: "CHM 100"
lens: "Helios 44M-4"
size: "12.7x17.8"
albums: 
    - "morocco"
resources:
    - name: photo
      type: "image"
      src: "morocco-004-05-1500.jpg"
---
