---
title: "000.26"
date: 2019-06-20T10:36:26+02:00
film: "HP5 PLUS"
lens: "Helios 44M-4"
size: "12.7x17.8"
albums: 
    - "leipzig"
resources:
    - name: photo
      type: "image"
      src: "leipzig-000-26-1500.jpg"
---
