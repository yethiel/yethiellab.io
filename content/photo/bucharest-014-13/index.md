---
title: "014.13"
date: 2020-05-03T12:43:05+02:00
film: "CHM 100"
lens: "Soligor S/M Zoom+Macro 35-70mm f/3.5-4.5"
size: "12.7x17.8"
albums: 
    - "bucharest"
resources:
    - name: photo
      type: "image"
      src: "bucharest-014-13-1500.jpg"
---

