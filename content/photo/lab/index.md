---
title: "lab"
date: 2019-05-29T11:17:45+02:00
film: "none"
lens: "pinhole"
size: "10.5x14.8"
albums: 
    - "experimental"
resources:
    - name: photo
      type: "image"
      src: "experimental-lab-1500.jpg"
---
