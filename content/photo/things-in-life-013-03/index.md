---
title: "013.03"
date: 2020-05-02T12:12:20+02:00
film: "CHM 400"
lens: "Porst Color Reflex MC Auto 55mm f/1.4"
size: "18x24"
albums: 
    - "things-in-life"
resources:
    - name: photo
      type: "image"
      src: "things-in-life-013-03-1500.jpg"
---


