---
title: "014.15"
date: 2020-05-02T22:49:48+02:00
film: "CHM 100"
lens: "Soligor S/M Zoom+Macro 35-70mm f/3.5-4.5"
size: "28x24"
albums: 
    - "bucharest"
resources:
    - name: photo
      type: "image"
      src: "bucharest-014-15-1500.jpg"
---
