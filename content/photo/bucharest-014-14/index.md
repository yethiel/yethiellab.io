---
title: "014.14"
date: 2020-03-23T22:49:47+02:00
film: "CHM 100"
lens: "Soligor S/M Zoom+Macro 35-70mm f/3.5-4.5"
size: "12.7x17.8"
albums: 
    - "bucharest"
resources:
    - name: photo
      type: "image"
      src: "bucharest-014-14-1500.jpg"
---
