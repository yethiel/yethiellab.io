---
title: "EP: Toneskipping and Earth Overtones A"
date: 2020-06-26T09:40:01+02:00
color: "#94583c"
album_id: "560778765"
---

An ambient / drone split EP with my friend Yseto.

<!--more-->


<iframe style="border: 0; width: 350px; height: 527px;" src="https://bandcamp.com/EmbeddedPlayer/album=560778765/size=large/bgcol=ffffff/linkcol=94583c/transparent=true/" seamless><a href="https://yethiel.bandcamp.com/album/toneskippping-and-earth-overtones-a">Toneskippping and Earth Overtones A by Marvin Thiel and Yseto</a></iframe>



- [Listen to the EP on Bandcamp](https://yethiel.bandcamp.com/album/toneskippping-and-earth-overtones-a)
- [Listen to the EP on Spotify](https://open.spotify.com/album/5Mb4WyjaFKKChah0a15XaS)
