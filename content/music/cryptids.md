---
title: "EP: Cryptids"
date: 2020-04-26T09:40:01+02:00
color: "#606060"
album_id: "2434583533"
---

This is an experimental EP in which I used a custom synthesizer setup (YETI 110m).

<!--more-->


<iframe style="border: 0; width: 350px; height: 786px;" src="https://bandcamp.com/EmbeddedPlayer/album=2434583533/size=large/bgcol=ffffff/linkcol=#606060/transparent=true/" seamless><a href="http://yethiel.bandcamp.com/album/at-7shots-coffee">Cryptids by Marvin Thiel</a></iframe>

- [Listen to the EP on Bandcamp](https://yethiel.bandcamp.com/album/cryptids)
- [Listen to the EP on Spotify](https://open.spotify.com/album/7ErHwRQHl2nOlNv8F14inc)
