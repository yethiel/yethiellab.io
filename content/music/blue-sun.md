---
title: "Album: Blue Sun"
date: 2020-03-13T09:40:01+02:00
color: "#3C63B0"
album_id: "3893379444"
---

This album is a collection of live recordings and arrangements made with my KORG Volca Modular in the beginning of 2020.


<!--more-->


<iframe style="border: 0; width: 350px; height: 687px;" src="https://bandcamp.com/EmbeddedPlayer/album=3893379444/size=large/bgcol=ffffff/linkcol=333333/transparent=true/" seamless><a href="http://yethiel.bandcamp.com/album/blue-sun">Blue Sun by Marvin Thiel</a></iframe>

The album includes the full 11-minute live recording of "Zenith" as a bonus track.


- [Listen to the Album on Bandcamp](https://yethiel.bandcamp.com/album/blue-sun)
