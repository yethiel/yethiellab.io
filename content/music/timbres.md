---
title: "Album: Timbres"
date: 2021-04-20T21:58:46+01:00
draft: false
color: "#e99708"
album_id: "2010491373"
---

Experimental synthesizer music.

<!--more-->


<iframe style="border: 0; width: 350px; height: 470px;" src="https://bandcamp.com/EmbeddedPlayer/album=2010491373/size=large/bgcol=ffffff/linkcol=e99708/tracklist=false/transparent=true/" seamless><a href="https://yethiel.bandcamp.com/album/timbres">Timbres by Yethiel</a></iframe>



- [Preorder on Bandcamp](https://yethiel.bandcamp.com/album/timbres)

