---
title: "Single: Yseto + Yethiel - Owt"
date: 2020-03-31T09:40:01+02:00
color: "#B74C36"
album_id: "108282680"
---

Recording of a Jam with Yseto. You can hear his modular synth setup including a Pittsburgh Lifeforms SV-1 and my Volca Modular.


<!--more-->


<iframe style="border: 0; width: 350px; height: 470px;" src="https://bandcamp.com/EmbeddedPlayer/album=108282680/size=large/bgcol=ffffff/linkcol=B74C36/tracklist=false/transparent=true/" seamless><a href="http://yethiel.bandcamp.com/album/owt">Owt by Yseto + Yethiel</a></iframe>


- [Listen to the single on Bandcamp](https://yethiel.bandcamp.com/album/owt)
