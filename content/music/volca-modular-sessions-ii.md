---
title: "Album: Volca Modular Sessions II"
date: 2020-09-27T09:40:01+02:00
color: "#45443F"
album_id: "3210099563"
---

This is the second part of improvisations I did with the KORG Volca Modular. The album is available on Spotify and other platforms under the name "Modular Sessions II".

<!--more-->


<iframe style="border: 0; width: 350px; height: 687px;" src="https://bandcamp.com/EmbeddedPlayer/album=3210099563/size=large/bgcol=ffffff/linkcol=45443F/transparent=true/" seamless><a href="http://yethiel.bandcamp.com/album/volca-modular-sessions-ii">Volca Modular Sessions II by Yethiel</a></iframe>



- [Listen to the Album on Bandcamp](https://yethiel.bandcamp.com/album/volca-modular-sessions-ii)
- [Listen to the Album on Spotify](https://open.spotify.com/album/53IoALXekYEc13WqPiA1yj)
