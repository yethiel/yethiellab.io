---
title: "Live Album: At 7shots Coffee"
date: 2020-03-16T09:40:01+02:00
color: "#564530"
album_id: "634381020"
---

This album is a live recording of a performance on a KORG Volca Modular from March 16, 2020 at the 7shots café. The album is dedicated to the café and the people who spend their time there.


<!--more-->


<iframe style="border: 0; width: 350px; height: 786px;" src="https://bandcamp.com/EmbeddedPlayer/album=634381020/size=large/bgcol=ffffff/linkcol=#564530/transparent=true/" seamless><a href="http://yethiel.bandcamp.com/album/at-7shots-coffee">At 7shots Coffee by Marvin Thiel</a></iframe>

- [Listen to the Album on Bandcamp](https://yethiel.bandcamp.com/album/at-7shots-coffee)
