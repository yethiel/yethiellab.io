---
title: "Album: Volca Modular Sessions"
date: 2020-04-12T09:40:01+02:00
color: "#2b7325"
album_id: "2017650394"
---

This is a collection of improvisations I did with the KORG Volca Modular.

<!--more-->


<iframe style="border: 0; width: 350px; height: 687px;" src="https://bandcamp.com/EmbeddedPlayer/album=2017650394/size=large/bgcol=ffffff/linkcol=2b7325/transparent=true/" seamless><a href="http://yethiel.bandcamp.com/album/volca-modular-sessions">Volca Modular Sessions by Marvin Thiel</a></iframe>

The album includes an earlier version of "For Paula" as a bonus track.


- [Listen to the Album on Bandcamp](https://yethiel.bandcamp.com/album/volca-modular-sessions)
- [Listen to the Album on Spotify](https://open.spotify.com/album/19mdpbQ83hwIgKKoIUtote)
