---
title: "Album Release: Yseto - Transit to Now"
date: 2020-01-20T11:52:02+02:00
categories: music
resources:
    - name: album
      type: "image"
      src: "transit-to-now-cdr.jpg"
color: "#714785"
album_id: "3600240126"
---

Mastering and publishing of Yseto's debut album _Transit to Now_, as well as management of social media accounts.

<!--more-->

- [Bandcamp](https://yseto.bandcamp.com/album/transit-to-now)
- [Soundcloud](https://soundcloud.com/yseto)

## Bandcamp

<iframe style="border: 0; width: 500px; height: 439px;" src="https://bandcamp.com/EmbeddedPlayer/album=3600240126/size=large/bgcol=ffffff/linkcol=0687f5/artwork=small/transparent=true/" seamless><a href="http://yseto.bandcamp.com/album/transit-to-now">Transit to Now by Yseto</a></iframe>
## Physical Release

A small number of self-released CD-Rs with hand-crafted paper sleeves were produced.


[![physical release](transit-to-now-cdr_500.jpg)](transit-to-now-cdr.jpg)
