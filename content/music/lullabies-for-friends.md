---
title: "Album: Lullabies for Friends"
date: 2020-09-27T09:40:01+02:00
color: "#992233"
album_id: "3502695388"
---

This album is meant to carry the feeling of deep friendship and the wonders that happen when you spend time with them.

<!--more-->

The album starts with rather rhythmic tracks and slowly develops into a calm bed of drones. I called this album "Lullabies for Friends" because I played all of these tracks while my friends were either relaxing or falling asleep to the music. All tracks were improvised and recorded live.

The album's release date is October 17th 2020 after which it will be available on Bandcamp, Spotify and many other platforms.

<iframe style="border: 0; width: 350px; height: 687px;" src="https://bandcamp.com/EmbeddedPlayer/album=3502695388/size=large/bgcol=ffffff/linkcol=45443F/transparent=true/" seamless><a href="http://yethiel.bandcamp.com/album/lullabies-for-friends">Lullabies for Friends by Yethiel</a></iframe>



- [Listen to the Album on Bandcamp](https://yethiel.bandcamp.com/album/lullabies-for-friends)
- [Listen to the Album on Spotify](https://open.spotify.com/album/0tMCp55JzyPoh3z148bVgG)