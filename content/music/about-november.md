---
title: "Album: About November"
date: 2020-12-05T21:58:46+01:00
draft: false
color: "#343434"
album_id: "3886292907"
---

Yseto and I recorded this album in early November using a multitrack recorder.

<!--more-->

This is the first album that features the Ouroboros Alea in addition to the Volca Modular. Yseto joins in with an Elektron Model:Samples filled with tons of self-made samples. All tracks were recorded in one day using a tape recorder (Fostex X-26). This approach was new to me, previously we used Yseto's equipment and giant mixer.  
Using an overly mid-range looking recorder with all knobs and faders available within reach makes performing and mixing at the same time much more fun.

<iframe style="border: 0; width: 350px; height: 687px;" src="https://bandcamp.com/EmbeddedPlayer/album=3886292907/size=large/bgcol=ffffff/linkcol=343434/transparent=true/" seamless><a href="http://yethiel.bandcamp.com/album/about-november">About November by Yethiel and Yseto</a></iframe>



- [Listen to the Album on Bandcamp](https://yethiel.bandcamp.com/album/about-november)
- [Listen to the Album on Spotify](https://open.spotify.com/album/5jkkJUZQ0Qc5IRA7SdJB1w)