---
title: "Album Release: Yseto - An Isle For You"
date: 2020-03-30T11:52:02+02:00
categories: music
color: "#B558A5"
album_id: "3520957627"
---

Mastering and publishing of Yseto's second album _An Isle for You_.

<!--more-->

- [Bandcamp](https://yseto.bandcamp.com/album/an-isle-for-you)
- [Soundcloud](https://soundcloud.com/yseto)

## Bandcamp

<iframe style="border: 0; width: 350px; height: 753px;" src="https://bandcamp.com/EmbeddedPlayer/album=3520957627/size=large/bgcol=ffffff/linkcol=f171a2/transparent=true/" seamless><a href="http://yseto.bandcamp.com/album/an-isle-for-you">An Isle for You by Yseto</a></iframe>
