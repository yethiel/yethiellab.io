---
title: "Patching Flowers"
date: 2020-10-11
categories: projects
color: "#8D1628"
---

Patching Flowers is a group of artists/musicians and functions an independent record label for ambient, electronic and experimental music.

<!--more-->

It was founded on the 11th October 2020.

In 2020 I started making music after being inspired by my then flatmate [Yseto](https://yseto.bandcamp.com) and a seminar I took at uni called "Psychoacoustic Experiments in the Studio". Since Yseto is working with a modular synthesizer and the seminar focused on working with and designing modular synthesizers, I picked up a [Volca Modular](https://www.korg.com/de/products/dj/volca_modular/). Despite them calling it a micro modular synthesizer, its potential seemed to be much bigger than any [Eurorack](https://en.wikipedia.org/wiki/Eurorack) system four times the price.  
The Volca Modular still is my main synthesizer and I started modifying it to use [banana jacks](https://en.wikipedia.org/wiki/Banana_jack) rather than the tiny millimeter-sized connectors.

Yseto and I continued to work on our projects together and decided to open an outlet for our work. Our plan is to work with more people and build a supporting community for everyone interested in experimental electronic music.

I wrote the website and its design using plain HTML and CSS. The logo was made by Lara Moenikes.

- [www.patchingflowers.com](https://www.patchingflowers.com)
- [Instagram: patchingflowers](https://www.instagram.com/patchingflowers)
