---
title: "Pizzacalc"
date: 2019-05-28T09:40:01+02:00
color: "#508D2F"
---

A single-page calculator for the PizzaLab.

<!--more-->

- [Project Page](https://yethiel.gitlab.io/pizzacalc)

<img class="img-decorate" src="/project/pizzacalc.png">
