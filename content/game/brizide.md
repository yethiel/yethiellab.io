---
title: "BriZide"
date: 2019-05-28T09:41:35+02:00
color: "#A37737"
---

A futuristic racing game that I made to find out a good structure for a user-modifiable game.
<!--more-->
Levels, graphics and even game modes are accessible as files.



- [GitLab (Source)](https://gitlab.com/yethiel/BriZide)

<iframe src="https://itch.io/embed/393856?linkback=true&amp;bg_color=1e1e1e&amp;fg_color=e3e3e3&amp;link_color=d6a600&amp;border_color=4c4c4c" width="552" height="167" frameborder="0"></iframe>
