---
title: "Re-Volt / RVGL"
date: 2019-05-28T09:42:36+02:00
draft: false
color: "#B5422A"
---

Additions to the RVGL code and creation of the project website.

<!--more-->

- [rvgl.re-volt.io](https://rvgl.re-volt.io)

I added some features to the game, for example a way for users to modify the physics properties of level surfaces ([properties.txt](https://yethiel.gitlab.io/RVDocs/#level-properties)).