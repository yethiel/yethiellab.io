---
title: "about"
date: 2019-05-27T17:08:57+02:00
color: "#9b351c"
---

I'm currently studying Digital Humanities in Leipzig.  
Interests: Music, specialty coffee, film photography, synthesizers and old speakers, good software, nice and accessible websites.


---

Stay safe and take care of people around you. Please wear a mask where asked and get vaccinated if you can (if not for yourself, do it for your friends, family and everyone you encounter in public transport or while running errands).

---

**Mail**: [self@yethiel.com](mailto:self@yethiel.com)  

**[GitLab](https://gitlab.com/yethiel)**  
**[Bandcamp](https://yethiel.bandcamp.com)**  
