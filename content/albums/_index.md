---
title: "Photography"
date: 2020-03-31T09:40:01+02:00
---

I develop films at home and also make prints with a Durst enlarger. Here you can find scans of the best prints.
