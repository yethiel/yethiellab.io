window.addEventListener('load', function () {
    preparePlayer();
})


function preparePlayer() {
    var playerDivs = document.getElementsByClassName("audio-player-wrapper");

    // Exists if there is only one player on the page
    if (playerDivs.length == 1) {
        return;
    }

    // Adds event listeners to all tracks to play through all of them
    for (var i=0; i < playerDivs.length; i++) {
        let player = playerDivs[i].getElementsByClassName("audio-player")[0];
        let nextPlayer = playerDivs[0].getElementsByClassName("audio-player")[0];
        if (i < playerDivs.length-1) {
            nextPlayer = playerDivs[i+1].getElementsByClassName("audio-player")[0];
        }
        player.addEventListener("ended", function() {
            nextPlayer.play();
        });
    }
}